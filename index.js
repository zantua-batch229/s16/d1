//console.log("Go!");

let x = 5;
let y = 1;

let sum = x+y; //6
sum = sum+2;  //8
console.log("Final result is : ");
console.log(sum+3); //11

let difference = x-y;

console.log(difference);

let product = x*y;
console.log("Result of multiplication operator: " + difference);


let quotaient = x/y;
console.log("Result of division operator: " + quotaient);

//modulus (%)
//get the remainder from divided values

x = 14;
y = 5;

let remainder = x % y;
// gets the remainder of 14 divided by 5
console.log("Result of modulo operator: " + remainder);


// ASSIGNMENT OPERATOR (=)
//Basic assignment

let	assignmentNumber = 8;

assignmentNumber = assignmentNumber + 2;

//same as above
assignmentNumber += 2;

console.log("Result of assignment operator: " + assignmentNumber);

assignmentNumber -= 2;

console.log("Result of subtraction operator: " + assignmentNumber);
assignmentNumber *= 2;

console.log("Result of multiplication operator: " + assignmentNumber);
assignmentNumber /= 2;

console.log("Result of division operator: " + assignmentNumber);

//multiple operators and parenthesis
let mdas = 1+2-3*4/5;
console.log(mdas);

let pemdas = 1+(2-3)*(4/5);
console.log(pemdas);

pemdas = (1+(2-3))*(4/5);
console.log(pemdas);

//incrementation (++) and decrementation (--)

let z=2;
// ++z added 1 to its original vale
let increment = ++z;

console.log(increment); //3
console.log(z); //3


increment = z++;

console.log(increment); //4
console.log(z); //5

//type coercion

let numA = "10";
let numB = 12;

let coerc = numA+numB;
console.log(coerc);
console.log(typeof coerc);

//false = 0, true = 1
let numE = true + 2;
console.log(numE);

//comparison operator

//equality operator (==)
//checks 2 operands if they are equal/have the same content
//may return boolean value;

//inequality operator (!-)
//checks 2 operands if they are NOT equal/DO NOT have the same content
//may return boolean value;

let juan = "juan";
console.log(true == 1); //true
console.log(true == 2); //false
console.log(1 == "1"); //true

//strict equality operator
//copares the content and the datatype
console.log(1 === "1"); //false

//strict inequality operator (!==)

console.log(1 !== "1"); //true

//relational operator - gt (>), gt(<), gt(<=), gt(>=)

let a = 50;
let b= 65;
let isGreaterThan = z > b;
console.log(isGreaterThan);

let numStr = "30";
console.log(a > numStr); //true
console.log(numStr < a); //true


//Logical Operators

let isLegalAge = true;
let isRegistered = false;

//Logical And Operator (&&)
let allRequirementMet = isLegalAge && isRegistered;

console.log(allRequirementMet); //false

isLegalAge = true;
isRegistered = true;
console.log(allRequirementMet); //true


isLegalAge = true;
isRegistered = false;
allRequirementMet = isLegalAge && !isRegistered;
console.log(allRequirementMet); //true

//logical OR operator (||)
allRequirementMet = isLegalAge || isRegistered;
console.log(allRequirementMet); //true

//logical NOT operator (!)
allRequirementMet = !isRegistered;
console.log(allRequirementMet); //true



